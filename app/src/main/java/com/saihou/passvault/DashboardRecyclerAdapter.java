package com.saihou.passvault;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class DashboardRecyclerAdapter extends RecyclerView.Adapter<DashboardRecyclerAdapter.PasswordViewHolder>
                implements Filterable {
    private List<Password> dataset;
    private List<Password> filteredDataset;

    Fragment fragment;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class PasswordViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        public TextView aliasTextView;
        public TextView encryptPassView;
        public ImageButton editButton;
        public ImageButton deleteButton;

        public PasswordViewHolder(View itemView) {
            super(itemView);

            aliasTextView = itemView.findViewById(R.id.item_password_alias);
            encryptPassView = itemView.findViewById(R.id.item_password_encrypted);
            editButton = itemView.findViewById(R.id.item_password_edit);
            deleteButton = itemView.findViewById(R.id.item_password_delete);
        }
    }

    public DashboardRecyclerAdapter(List<Password> passwords, Fragment fragment) {
        dataset = passwords;
        filteredDataset = dataset;
        this.fragment = fragment;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PasswordViewHolder onCreateViewHolder(ViewGroup parent,
                                                 int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_password_view, parent, false);

        PasswordViewHolder vh = new PasswordViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(PasswordViewHolder holder, int position) {
        Password pass = filteredDataset.get(position);

        holder.aliasTextView.setText(pass.alias);
        holder.encryptPassView.setText(pass.encryptedPass);
        holder.deleteButton.setOnClickListener(view -> {
            // Build a confirmation prompt for delete action
            AlertDialog.Builder builder = new AlertDialog.Builder(fragment.getActivity());
            builder.setTitle(R.string.confirm_delete);
            builder.setMessage(R.string.cannot_be_undone);
            builder.setPositiveButton(R.string.yes, (dialog, which) -> {
                dialog.dismiss();

                // User confirms delete action
                int removeIndex = holder.getAdapterPosition();
                ((MainActivity) fragment.getActivity()).removePassword(pass);
                notifyItemRemoved(removeIndex);
                ((DashboardFragment) fragment).refresh();
            });

            builder.setNegativeButton(R.string.no, (dialog, which) -> {
                // Do nothing
                dialog.dismiss();
            });

            AlertDialog alert = builder.create();
            alert.show();
        });

        holder.editButton.setOnClickListener(view -> {
            ((DashboardFragment) fragment).onViewMorePassword(pass);
        });
    }

    // Return the size of filtered dataset
    @Override
    public int getItemCount() {
        return filteredDataset.size();
    }

    public int getRawItemCount() {
        return dataset.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults filterResults = new FilterResults();

                String searchString = charSequence.toString();
                if (searchString.isEmpty()) {
                    // Return the original list
                    filteredDataset = dataset;
                    filterResults.values = filteredDataset;
                } else {
                    // Filter the dataset
                    List<Password> searchResults = new ArrayList<>();
                    for (Password pass : dataset) {
                        String name = pass.alias;
                        String desc = pass.desc;
                        if (name.toLowerCase().contains(searchString.toLowerCase())
                                || desc.toLowerCase().contains(searchString.toLowerCase())) {
                            searchResults.add(pass);
                        }
                    }
                    filteredDataset = searchResults;
                    filterResults.values = filteredDataset;
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredDataset = (List<Password>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
