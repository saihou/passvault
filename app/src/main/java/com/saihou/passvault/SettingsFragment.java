package com.saihou.passvault;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.Unbinder;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import org.w3c.dom.Text;

import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SettingsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class SettingsFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    @BindView(R.id.btn_set_masterkey)
    Button setMasterButton;
    @BindView(R.id.masterkey)
    TextView masterText;
    @BindView(R.id.freq_spinner)
    Spinner freqSpinner;
    @BindView(R.id.checkbox_fingerprint)
    CheckBox fingerprintCheckbox;
    @BindView(R.id.fingerprint)
    TextView fingerprintText;
    @BindView(R.id.last_auth)
    TextView lastAuthText;

    @BindView(R.id.randomizer_length)
    SeekBar lengthSeekbar;
    @BindView(R.id.randomizer_length_text)
    TextView lengthText;
    @BindView(R.id.ruleset_upper)
    CheckBox upperCheckbox;
    @BindView(R.id.ruleset_lower)
    CheckBox lowerCheckbox;
    @BindView(R.id.ruleset_numbers)
    CheckBox numbersCheckbox;
    @BindView(R.id.ruleset_special)
    CheckBox specialCheckbox;


    MainActivity activity;
    boolean isMasterKeySet = false;
    private Unbinder unbinder;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        unbinder = ButterKnife.bind(this, view);

        activity = (MainActivity) getActivity();
        if (activity != null) {
            isMasterKeySet = activity.hasSetMasterKey();
        }

        // Note: lengthSeekbar min is assumed to be 0
        lengthSeekbar.setMax(Constants.RANDOMIZER_MAX_LENGTH - Constants.RANDOMIZER_MIN_LENGTH);
        lengthSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String displayText  = String.valueOf(progress + Constants.RANDOMIZER_MIN_LENGTH);
                String formatted    = String.format(getResources().getString(R.string.random_length_desc), displayText);
                lengthText.setText(formatted);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Don't need this functionality
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (activity != null) {
                    activity.setRandomizerLength(seekBar.getProgress() + Constants.RANDOMIZER_MIN_LENGTH);
                }
            }
        });

        refresh();

        return view;
    }

    private void refresh() {
        refresh(isMasterKeySet);
    }

    private void refresh(boolean isMasterKeySet) {
        if (activity == null) return;

        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());

        // Master Key section
        if (isMasterKeySet) {
            // Master Key is set. Disable the set button
            String encryptedMaster = appSharedPrefs.getString(Constants.PREF_MASTER, getResources().getString(R.string.settings_not_set));
            masterText.setText(encryptedMaster);
        } else {
            // Master Key not set. Don't need to do anything as the default layout xml is already set up for this.
        }

        setMasterButton.setEnabled(!isMasterKeySet);
        freqSpinner.setEnabled(isMasterKeySet);
        fingerprintCheckbox.setEnabled(isMasterKeySet);

        // Fingerprint section
        boolean useFingerprints = MasterPassAuthentication.getAuthenticationType() == MasterPassAuthentication.AuthenticateType.FINGERPRINT;
        fingerprintCheckbox.setChecked(useFingerprints);
        fingerprintText.setText(useFingerprints ? R.string.allow_fingerprint_desc : R.string.disallow_fingerprint_desc);

        // Frequency section
        int freqSetting = MasterPassAuthentication.getAuthenticateInterval();
        freqSpinner.setSelection(freqSetting);
        if (appSharedPrefs.contains(Constants.PREF_LAST_AUTH)) {
            Date lastAuthDate   = new Date(appSharedPrefs.getLong(Constants.PREF_LAST_AUTH, 0));
            Date currentDate    = new Date(System.currentTimeMillis());

            long minutesApart   = MasterPassAuthentication.diffBetweenTwoDates(lastAuthDate, currentDate);
            String formatted    = String.format(getResources().getString(R.string.last_authenticated), String.valueOf(minutesApart));
            lastAuthText.setText(formatted);
        } else {
            lastAuthText.setText(R.string.last_authenticated_none);
        }

        // Length section
        int targetLength = appSharedPrefs.getInt(Constants.PREF_RANDOM_LENGTH, Constants.RANDOMIZER_MAX_LENGTH) - Constants.RANDOMIZER_MIN_LENGTH;
        lengthSeekbar.setProgress(targetLength);

        // Ruleset section
        upperCheckbox.setChecked(PasswordRandomizer.getRuleset(Constants.PREF_RANDOM_UPPER));
        lowerCheckbox.setChecked(PasswordRandomizer.getRuleset(Constants.PREF_RANDOM_LOWER));
        numbersCheckbox.setChecked(PasswordRandomizer.getRuleset(Constants.PREF_RANDOM_NUMBER));
        specialCheckbox.setChecked(PasswordRandomizer.getRuleset(Constants.PREF_RANDOM_SPECIAL));
    }

    @OnItemSelected(R.id.freq_spinner)
    void onFreqChanged(int position) {
        if (activity != null) {
            activity.setAuthFrequency(position);
        }
    }

    @OnCheckedChanged(R.id.checkbox_fingerprint)
    void onFingerprintChange(CompoundButton button, boolean isChecked) {
        if (activity != null) {
            activity.setUseFingerprints(isChecked);
        }
        fingerprintText.setText(isChecked ? R.string.allow_fingerprint_desc : R.string.disallow_fingerprint_desc);
    }

    @OnClick(R.id.btn_set_masterkey)
    public void onClickSetMasterkey() {
        if (activity == null) return;

        LayoutInflater layoutInflater = LayoutInflater.from(activity);
        final View popupMasterSetView = layoutInflater.inflate(R.layout.popup_masterkey_set, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.masterkey_set_title);
        builder.setMessage(R.string.masterkey_set_message);

        final EditText masterkeyText = popupMasterSetView.findViewById(R.id.alert_master_set);
        final EditText masterKeyConfirmText = popupMasterSetView.findViewById(R.id.alert_master_confirm);

        builder.setPositiveButton(R.string.ok, null); // Set to null listener here, then override it later. See below for details.

        builder.setNegativeButton(R.string.cancel, (dialog, which) -> {
            dialog.dismiss();
        });

        AlertDialog alert = builder.create();
        alert.setView(popupMasterSetView);
        alert.show();

        // Doing things slightly different here - by default, any button (positive/negative) will dismiss the dialog. But if Master Keys are not same,
        // we don't want to dismiss. Thus, override the behavior of onclick of the positive button here AFTER show(), instead of setting it beforehand.
        alert.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
            String master1 = masterkeyText.getText().toString();
            String master2 = masterKeyConfirmText.getText().toString();

            if (master1.length() < Constants.MASTER_KEY_MIN_LENGTH || master2.length() < Constants.MASTER_KEY_MIN_LENGTH) {
                String msg = String.format(getResources().getString(R.string.masterkey_too_short), String.valueOf(Constants.MASTER_KEY_MIN_LENGTH));
                Snackbar.make(popupMasterSetView, msg, Snackbar.LENGTH_SHORT).show();
            } else if (master1.equals(master2)) {
                setNewMasterPassword(master1);
                alert.dismiss();
            } else {
                Snackbar.make(popupMasterSetView, R.string.masterkey_wrong, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    void setNewMasterPassword(String master) {
        if (activity != null) {
            activity.setNewMasterPassword(master);
            isMasterKeySet = true;
            refresh();
        }
    }

    @OnCheckedChanged(R.id.ruleset_upper)
    void onUpperChanged(CompoundButton button, boolean isChecked) {
        if (activity != null) {
            activity.setRandomizerRuleset(Constants.PREF_RANDOM_UPPER, isChecked);
        }
    }

    @OnCheckedChanged(R.id.ruleset_lower)
    void onLowerChanged(CompoundButton button, boolean isChecked) {
        if (activity != null) {
            activity.setRandomizerRuleset(Constants.PREF_RANDOM_LOWER, isChecked);
        }
    }

    @OnCheckedChanged(R.id.ruleset_numbers)
    void onNumberChanged(CompoundButton button, boolean isChecked) {
        if (activity != null) {
            activity.setRandomizerRuleset(Constants.PREF_RANDOM_NUMBER, isChecked);
        }
    }

    @OnCheckedChanged(R.id.ruleset_special)
    void onSpecialChanged(CompoundButton button, boolean isChecked) {
        if (activity != null) {
            activity.setRandomizerRuleset(Constants.PREF_RANDOM_SPECIAL, isChecked);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
