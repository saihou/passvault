package com.saihou.passvault;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DashboardFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class DashboardFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    MainActivity activity;

    @BindView(R.id.dashboard_recycler_view)
    RecyclerView recyclerView;
    private DashboardRecyclerAdapter mAdapter;

    @BindView(R.id.dashboard_nothing_yet)
    TextView nothingText;

    private Unbinder unbinder;

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume(){
        super.onResume();
        refresh();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Don't really need to implement Submit, as the search filter is done continuously
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    public void refresh() {
        mAdapter.notifyDataSetChanged();
        nothingText.setVisibility(mAdapter.getRawItemCount() == 0 ? View.VISIBLE : View.INVISIBLE);
    }

    @OnClick(R.id.add_new_entry)
    public void onFabClicked() {
        if (activity == null) return;

        boolean hasSetMasterKey = activity.hasSetMasterKey();
        if (hasSetMasterKey) {
            Intent intent = new Intent(getActivity(), NewPasswordActivity.class);
            activity.startActivityForResult(intent, Constants.NEW_PASSWORD_ACTIVITY_RESULT);
        } else {
            // Master key is not set. Tell user to the Settings page to set.
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(R.string.masterkey_not_set_title);
            builder.setMessage(R.string.masterkey_not_set_message);
            builder.setPositiveButton(R.string.ok, (dialog, which) -> {
                dialog.dismiss();
            });

            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void onViewMorePassword(Password p) {
        if (activity == null) return;

        Intent intent = new Intent(getActivity(), EditPasswordActivity.class);
        intent.putExtra(Constants.CREATE_PASS_NAME, p.getIdentifier());
        intent.putExtra(Constants.CREATE_PASS_WEBSITE, p.getDesc());
        intent.putExtra(Constants.CREATE_PASS_PASS, p.getEncryptedPass());
        intent.putExtra(Constants.EDIT_PASS_IV, p.getIv());

        activity.startActivityForResult(intent, Constants.EDIT_PASSWORD_ACTIVITY_RESULT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        unbinder = ButterKnife.bind(this, view);

        activity = (MainActivity) getActivity();

        // Use a linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);

        // Specify an adapter
        mAdapter = new DashboardRecyclerAdapter(activity.getData(), this);
        recyclerView.setAdapter(mAdapter);

        // Decide whether to show "greeting" text or not
        nothingText.setVisibility(mAdapter.getRawItemCount() == 0 ? View.VISIBLE : View.INVISIBLE);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
