package com.saihou.passvault;

import java.util.Random;

public class PasswordRandomizer {
    // Utility class to generate random passwords

    final static String upperCaseChars  = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    final static String lowerCaseChars  = "abcdefghijklmnopqrstuvwxyz";
    final static String numberChars     = "0123456789";
    final static String specialChars    = "!@#$%^&*_+-=?~";

    private static boolean lower, upper, numbers, special;
    private static int targetLength;

    public static String getRandomPassword() {
        if (targetLength < 0) {
            return "";
        }

        if (!lower && !upper && !numbers && !special) {
            return ""; // Can't generate anything when all characters are disallowed!
        }

        String allowedChars         = "";
        if (lower) allowedChars     += lowerCaseChars;
        if (upper) allowedChars     += upperCaseChars;
        if (numbers) allowedChars   += numberChars;
        if (special) allowedChars   += specialChars;

        final Random random = new Random();
        StringBuilder resultBuilder = new StringBuilder();

        for (int i = 0; i < targetLength; ++i) {
            resultBuilder.append(allowedChars.charAt(random.nextInt(allowedChars.length())));
        }

        return resultBuilder.toString();
    }

    public static void setTargetLength(int length) {
        targetLength = length;
    }

    public static void setRuleset(boolean low, boolean upp, boolean num, boolean symbol) {
        lower = low;
        upper = upp;
        numbers = num;
        special = symbol;
    }

    public static void setRuleset(String key, boolean value) {
        switch (key) {
            case Constants.PREF_RANDOM_LOWER:
                lower = value;
                break;
            case Constants.PREF_RANDOM_UPPER:
                upper = value;
                break;
            case Constants.PREF_RANDOM_NUMBER:
                numbers = value;
                break;
            case Constants.PREF_RANDOM_SPECIAL:
                special = value;
                break;
            default:
                break;
        }
    }

    public static boolean getRuleset(String key) {
        switch (key) {
            case Constants.PREF_RANDOM_LOWER:
                return lower;
            case Constants.PREF_RANDOM_UPPER:
                return upper;
            case Constants.PREF_RANDOM_NUMBER:
                return numbers;
            case Constants.PREF_RANDOM_SPECIAL:
                return special;
            default:
                return true;
        }
    }
}
