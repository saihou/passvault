package com.saihou.passvault;

public interface IPasswordModifier {
    void onCancelClicked();

    void onSaveClicked();

    void onVisibilityClicked();

    void onRandomizePasswordClicked();

    boolean validateInput();

    boolean hasUnsavedChanges();
}
