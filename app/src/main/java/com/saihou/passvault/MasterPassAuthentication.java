package com.saihou.passvault;

import java.util.Date;

public class MasterPassAuthentication {
    // Utility class to keep track of when to authenticate, and how to authenticate

    // region AUTH_FREQ

    /*
        These values correspond to the array index in freq_spinner.xml
     */
    public static int EVERY_TIME    = 0;
    public static int ONE_MIN       = 1;
    public static int FIVE_MIN      = 2;
    public static int ONE_TIME      = 3;

    private static int authenticateInterval;

    public static void setAuthenticateInterval(int i) {
        authenticateInterval = i;
    }

    public static int getAuthenticateInterval() {
        return authenticateInterval;
    }

    // Returns the number of minutes between 2 Dates
    public static long diffBetweenTwoDates(Date d1, Date d2) {
        // Assert d2 is "later" than d1
        long diffMilliseconds = d2.getTime() - d1.getTime();

        return diffMilliseconds / 60000; // 1 min is 60000 milliseconds
    }
    // endregion


    // region AUTH TYPE
    private static AuthenticateType authenticateType;

    public enum AuthenticateType {
        PASSWORD,
        FINGERPRINT,
    }

    // Always called on startup, or when user changes it in settings.
    public static void setAuthenticationType(AuthenticateType t) {
        authenticateType = t;
    }

    public static AuthenticateType getAuthenticationType() {
        return authenticateType;
    }

    // endregion
}
