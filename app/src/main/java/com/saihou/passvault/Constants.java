package com.saihou.passvault;

public class Constants {

    public static final int NEW_PASSWORD_ACTIVITY_RESULT = 2;
    public static final int EDIT_PASSWORD_ACTIVITY_RESULT = 4;

    public static final String CREATE_PASS_NAME = "CREATE_NAME";
    public static final String CREATE_PASS_PASS = "CREATE_PASS";
    public static final String CREATE_PASS_WEBSITE = "CREATE_WEBSITE";
    public static final String EDIT_PASS_IV = "EDIT_IV";

    public static final String TRANSFORMATION = "AES/GCM/NoPadding";
    public static final String ANDROID_KEY_STORE = "AndroidKeyStore";
    public static final String ALIAS_CRYPTO = "PASSVAULTCRYPT";

    public static final String PREF_KEY = "SHARED_PREF_KEY"; // data key (for all the passwords)
    public static final String PREF_MASTER = "SHARED_PREF_MASTER_KEY"; // masterkey key
    public static final String PREF_MASTER_IV = "SHARED_PREF_MASTER_IV"; // masterkey iv key

    public static final String PREF_FINGERPRINTS = "SHARED_PREF_FINGERPRINTS";
    public static final String PREF_FREQUENCY = "SHARED_PREF_FREQUENCY";
    public static final String PREF_LAST_AUTH = "SHARED_PREF_LAST_AUTH";

    public static final String PREF_RANDOM_LENGTH = "SHARED_PREF_RANDOM_LENGTH";
    public static final String PREF_RANDOM_UPPER = "SHARED_PREF_RANDOM_UPPER";
    public static final String PREF_RANDOM_LOWER = "SHARED_PREF_RANDOM_LOWER";
    public static final String PREF_RANDOM_NUMBER = "SHARED_PREF_RANDOM_NUMBER";
    public static final String PREF_RANDOM_SPECIAL = "SHARED_PREF_RANDOM_SPECIAL";

    public static final int MASTER_KEY_MIN_LENGTH = 5;
    public static final int RANDOMIZER_MIN_LENGTH = 10;
    public static final int RANDOMIZER_MAX_LENGTH = 30;

}
