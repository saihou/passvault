package com.saihou.passvault;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.preference.PreferenceManager;
import android.util.Base64;
import android.view.MenuItem;

import java.io.IOException;
import java.lang.reflect.Type;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class MainActivity extends AppCompatActivity implements DashboardFragment.OnFragmentInteractionListener, SettingsFragment.OnFragmentInteractionListener {

    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    Fragment currentFragment;
    int page = 0;

    private Encryptor encryptor;

    List<Password> storedPasswords;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            if (page == item.getItemId()) return true; // same page;

            FragmentManager manager = getSupportFragmentManager();
            switch (item.getItemId()) {
                case R.id.navigation_dashboard:
                    currentFragment = new DashboardFragment();
                    manager.beginTransaction().replace(R.id.main_fragment, currentFragment).commit();
                    page = R.id.navigation_dashboard;
                    return true;
                case R.id.navigation_settings:
                    currentFragment = new SettingsFragment();
                    manager.beginTransaction().replace(R.id.main_fragment, currentFragment).commit();
                    page = R.id.navigation_settings;
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // Initialisation
        retrievePasswords();
        retrieveSettings();

        // Starting fragment is DashboardFragment
        FragmentManager manager = getSupportFragmentManager();
        currentFragment = new DashboardFragment();
        manager.beginTransaction().replace(R.id.main_fragment, currentFragment).commit();
        page = R.id.navigation_dashboard;

        try {
            encryptor = new Encryptor();
        } catch (CertificateException | NoSuchAlgorithmException | KeyStoreException |
                IOException e) {
            e.printStackTrace();
        }

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (Constants.NEW_PASSWORD_ACTIVITY_RESULT) : {
                if (resultCode == Activity.RESULT_OK) {
                    processActivityResult(data, false);
                }
                break;
            }

            case (Constants.EDIT_PASSWORD_ACTIVITY_RESULT) : {
                if (resultCode == Activity.RESULT_OK) {
                    processActivityResult(data, true);
                }
                break;
            }
        }
    }

    void processActivityResult(Intent data, boolean isEdit) {
        String identifier = data.getStringExtra(Constants.CREATE_PASS_NAME);
        String desc = data.getStringExtra(Constants.CREATE_PASS_WEBSITE);
        String pass = data.getStringExtra(Constants.CREATE_PASS_PASS);

        // Encrypt pass
        String encrypt = encryptText(pass, Constants.ALIAS_CRYPTO);
        byte[] iv = encryptor.getIv();

        if (isEdit) {
            editPassword(identifier, desc, encrypt, iv);
        } else {
            Password newPassword = new Password(identifier, desc, encrypt, iv);
            addPassword(newPassword);
        }
    }

    // region SETTINGS
    private void retrieveSettings() {
        boolean useFingerprints = getUseFingerprints();
        MasterPassAuthentication.setAuthenticationType(useFingerprints ?
                MasterPassAuthentication.AuthenticateType.FINGERPRINT : MasterPassAuthentication.AuthenticateType.PASSWORD);

        int freqSetting = getAuthFrequency();
        MasterPassAuthentication.setAuthenticateInterval(freqSetting);

        int targetLength = getRandomizerLength();
        PasswordRandomizer.setTargetLength(targetLength);

        boolean lower   = getRandomizerRuleset(Constants.PREF_RANDOM_LOWER);
        boolean upper   = getRandomizerRuleset(Constants.PREF_RANDOM_UPPER);
        boolean number  = getRandomizerRuleset(Constants.PREF_RANDOM_NUMBER);
        boolean special = getRandomizerRuleset(Constants.PREF_RANDOM_SPECIAL);
        PasswordRandomizer.setRuleset(lower, upper, number, special);
    }

    public void setAuthFrequency(int freq) {
        MasterPassAuthentication.setAuthenticateInterval(freq);

        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();

        prefsEditor.putInt(Constants.PREF_FREQUENCY, freq);
        prefsEditor.apply();
    }

    public int getAuthFrequency() {
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        return appSharedPrefs.getInt(Constants.PREF_FREQUENCY, 0);
    }

    public void setUseFingerprints(boolean useFingerprints) {
        MasterPassAuthentication.setAuthenticationType(useFingerprints ?
                MasterPassAuthentication.AuthenticateType.FINGERPRINT : MasterPassAuthentication.AuthenticateType.PASSWORD);

        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();

        prefsEditor.putBoolean(Constants.PREF_FINGERPRINTS, useFingerprints);
        prefsEditor.apply();
    }

    public boolean getUseFingerprints() {
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        return appSharedPrefs.getBoolean(Constants.PREF_FINGERPRINTS, false);
    }

    public void setRandomizerLength(int targetLength) {
        PasswordRandomizer.setTargetLength(targetLength);

        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();

        prefsEditor.putInt(Constants.PREF_RANDOM_LENGTH, targetLength);
        prefsEditor.apply();
    }

    public int getRandomizerLength() {
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        return appSharedPrefs.getInt(Constants.PREF_RANDOM_LENGTH, Constants.RANDOMIZER_MAX_LENGTH);
    }

    public void setRandomizerRuleset(String prefsKey, boolean allow) {
        if (prefsKey != Constants.PREF_RANDOM_UPPER && prefsKey != Constants.PREF_RANDOM_LOWER
            && prefsKey != Constants.PREF_RANDOM_NUMBER && prefsKey != Constants.PREF_RANDOM_SPECIAL) {
            return;
        }

        PasswordRandomizer.setRuleset(prefsKey, allow);

        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();

        prefsEditor.putBoolean(prefsKey, allow);
        prefsEditor.apply();
    }

    public boolean getRandomizerRuleset(String prefsKey) {
        if (prefsKey != Constants.PREF_RANDOM_UPPER && prefsKey != Constants.PREF_RANDOM_LOWER
                && prefsKey != Constants.PREF_RANDOM_NUMBER && prefsKey != Constants.PREF_RANDOM_SPECIAL) {
            return false;
        }

        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        return appSharedPrefs.getBoolean(prefsKey, true);
    }

    public boolean hasSetMasterKey() {
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        return appSharedPrefs.contains(Constants.PREF_MASTER);
    }

    public void setNewMasterPassword(String master) {
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();

        String encryptedMaster = encryptText(master, Constants.ALIAS_CRYPTO);;
        byte[] iv = encryptor.getIv();
        String ivString = Base64.encodeToString(iv, Base64.DEFAULT);
        prefsEditor.putString(Constants.PREF_MASTER, encryptedMaster);
        prefsEditor.putString(Constants.PREF_MASTER_IV, ivString);
        prefsEditor.apply();
    }

    // endregion

    // region PASSWORD
    void editPassword(String identifier, String website, String pass, byte[] iv) {
        for (int i = 0; i < storedPasswords.size(); ++i) {
            Password p = storedPasswords.get(i);
            if (identifier.equals(p.alias)) {
                p.desc = website;
                p.encryptedPass = pass;
                p.iv = iv;

                break;
            }
        }

        savePasswordsToDisk();

        //System.out.println("Edited existing password.");
    }

    void addPassword(Password p) {
        storedPasswords.add(p);

        savePasswordsToDisk();

        //System.out.println("Stored new password!");
    }

    public void removePassword(Password p) {
        storedPasswords.remove(p);

        savePasswordsToDisk();

        //System.out.println("Removed password.");
    }

    void savePasswordsToDisk() {
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();

        Gson gson = new Gson();
        String json = gson.toJson(storedPasswords);
        prefsEditor.putString(Constants.PREF_KEY, json);
        prefsEditor.apply();
    }

    void retrievePasswords() {
        //System.out.println("Retrieving passwords...");

        SharedPreferences appSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this.getApplicationContext());

        if (!appSharedPrefs.contains(Constants.PREF_KEY)) {
            storedPasswords = new ArrayList<Password>();
            return;
        }

        String json = appSharedPrefs.getString(Constants.PREF_KEY, "");
        Gson gson = new Gson();
        Type type = new TypeToken<List<Password>>(){}.getType();
        storedPasswords = gson.fromJson(json, type);
        //for (Password obj : storedPasswords) {System.out.println(obj);}
    }

    // endregion

    // region ENCRYPT
    public String encryptText(String textToEncrypt, String alias) {
        String result = "";
        try {
            final byte[] encryptedText = encryptor.encryptText(alias, textToEncrypt);
            result = (Base64.encodeToString(encryptedText, Base64.DEFAULT));
        } catch (UnrecoverableEntryException | NoSuchAlgorithmException | NoSuchProviderException |
                KeyStoreException | IOException | NoSuchPaddingException | InvalidKeyException e) {
        } catch (InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }
        return result;
    }
    // endregion


    public List<Password> getData() {
        return storedPasswords;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }
}
