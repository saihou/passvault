package com.saihou.passvault;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricPrompt;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.Date;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditPasswordActivity extends AppCompatActivity implements IPasswordModifier {

    private Decryptor decryptor;

    @BindView(R.id.btn_edit_edit)
    Button editButton;
    @BindView(R.id.edit_alias)
    TextView name;
    @BindView(R.id.edit_site)
    EditText website;
    @BindView(R.id.edit_password)
    EditText password;
    @BindView(R.id.edit_confirm_password)
    EditText confirmPassword;
    @BindView(R.id.edit_confirm_password_label)
    TextView confirmPasswordLabel;
    @BindView(R.id.btn_edit_visibility)
    ImageButton visibilityButton;
    @BindView(R.id.btn_edit_save_password)
    Button saveButton;

    boolean canEdit = false;
    String encryptedPass;
    byte[]iv;

    String oldDesc, oldPassword; // for storing the original texts, so that later on we can check for unsaved changes

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_password);
        ButterKnife.bind(this);

        setTitle(R.string.title_edit_password);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            decryptor = new Decryptor();
        } catch (CertificateException | NoSuchAlgorithmException | KeyStoreException |
                IOException e) {
            e.printStackTrace();
        }

        name.setText(getIntent().getStringExtra(Constants.CREATE_PASS_NAME));
        website.setText(getIntent().getStringExtra(Constants.CREATE_PASS_WEBSITE));
        encryptedPass = getIntent().getStringExtra(Constants.CREATE_PASS_PASS);
        password.setText(encryptedPass);
        iv = getIntent().getByteArrayExtra(Constants.EDIT_PASS_IV);

        saveButton.setEnabled(false);
        website.setEnabled(false);
        website.setFocusable(false);
        password.setEnabled(false);
        password.setFocusable(false);
        confirmPasswordLabel.setVisibility(View.INVISIBLE);
        confirmPassword.setVisibility(View.INVISIBLE);
        confirmPassword.setEnabled(false);
        confirmPassword.setFocusable(false);

        oldDesc = website.getText().toString();
    }

    @Override
    public void onBackPressed() {
        // For the Android back button on device
        onCancelClicked();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // For the back button on the top left corner
            case android.R.id.home:
                onCancelClicked();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_edit_edit)
    void onEditClicked() {
        if (canEdit) return;

        if (shouldAskAuth()) {
            if (MasterPassAuthentication.getAuthenticationType() == MasterPassAuthentication.AuthenticateType.PASSWORD) {
                // Show popup window to allow user to type in password

                LayoutInflater layoutInflater = LayoutInflater.from(this);
                final View popupMasterCheckView = layoutInflater.inflate(R.layout.popup_masterkey_check, null);
                final EditText masterkeyEditText = popupMasterCheckView.findViewById(R.id.alertEditText);

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.authentication);

                builder.setPositiveButton(R.string.ok, null); // Set to null listener here, then override it later. See below for details.

                builder.setNegativeButton(R.string.cancel, (dialog, which) -> {
                    dialog.dismiss();
                });

                AlertDialog alert = builder.create();
                alert.setView(popupMasterCheckView);
                alert.show();

                // Doing things slightly different here - by default, any button (positive/negative) will dismiss the dialog. But if Master Keys are not same,
                // we don't want to dismiss. Thus, override the behavior of onclick of the positive button here AFTER show(), instead of setting it beforehand.
                alert.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
                    if (masterkeyEditText != null && checkMasterPasswordMatches(masterkeyEditText.getText().toString())) {
                        alert.dismiss();
                        onSuccessfulAuthentication();
                        setLastAuth();
                    } else {
                        Snackbar.make(popupMasterCheckView, R.string.masterkey_wrong, Snackbar.LENGTH_SHORT).show();
                    }
                });


            } else if (MasterPassAuthentication.getAuthenticationType() == MasterPassAuthentication.AuthenticateType.FINGERPRINT) {
                // Show Biometric Prompt for fingerprint authentication

                BiometricPrompt.PromptInfo.Builder builder = new BiometricPrompt.PromptInfo.Builder();
                builder.setTitle(getResources().getString(R.string.authentication));
                builder.setSubtitle(getResources().getString(R.string.use_fingerprint));
                builder.setNegativeButtonText(getResources().getString(R.string.cancel));
                BiometricPrompt.PromptInfo promptInfo = builder.build();

                Executor newExecutor = Executors.newSingleThreadExecutor();
                BiometricPrompt biometricPrompt = new BiometricPrompt(this, newExecutor, new BiometricPrompt.AuthenticationCallback() {
                    @Override
                    public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                        super.onAuthenticationError(errorCode, errString);
                        // Don't need to do anything

//                        if (errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON) {
                            // User pressed canceled. Actually, don't really need to do anything now
//                        } else {
                            // Don't need to do anything
//                        }
                    }

                    @Override
                    public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                        super.onAuthenticationSucceeded(result);
                        runOnUiThread(() -> {
                            onSuccessfulAuthentication();
                            setLastAuth();
                        });
                    }

                    @Override
                    public void onAuthenticationFailed() {
                        super.onAuthenticationFailed();
                        // Don't need to do anything
                    }
                });

                biometricPrompt.authenticate(promptInfo);
            }
        } else {
            onSuccessfulAuthentication();
        }
    }

    void onCopyClicked(View v) {
        if (canEdit) {
            ClipboardManager clipboard = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
//            ClipData clip = ClipData.newPlainText("copy", oldPassword);
            ClipData clip = ClipData.newPlainText("copy", password.getText().toString());
            clipboard.setPrimaryClip(clip);
            Toast toast = Toast.makeText(getApplicationContext(), R.string.copied_to_clipboard, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    @Override
    @OnClick(R.id.btn_edit_save_password)
    public void onSaveClicked() {
        if (validateInput()) {
            saveDataToResultIntent();
            finish();
        }
    }

    @Override
    @OnClick(R.id.btn_cancel_edit_password)
    public void onCancelClicked() {
        if (canEdit && hasUnsavedChanges()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.return_to_dashboard);
            builder.setPositiveButton(R.string.yes, (dialog, which) -> {
                dialog.dismiss();
                finish();
            });

            builder.setNegativeButton(R.string.no, (dialog, which) -> {
                // Do nothing
                dialog.dismiss();
            });

            AlertDialog alert = builder.create();
            alert.show();
        } else {
            finish();
        }
    }

    @Override
    @OnClick(R.id.btn_edit_visibility)
    public void onVisibilityClicked() {
        int currentInputType = password.getInputType();
        boolean isCurrentlyHidden = currentInputType == (InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        int newInputType = isCurrentlyHidden ? InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD : (InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        visibilityButton.setImageResource(isCurrentlyHidden ? R.drawable.ic_visibility_24px : R.drawable.ic_visibility_off_24px);

        password.setInputType(newInputType);
        confirmPassword.setInputType(newInputType);
    }

    @Override
    @OnClick(R.id.btn_edit_randomize)
    public void onRandomizePasswordClicked() {
        if (canEdit) {
            String randomized = PasswordRandomizer.getRandomPassword();
            password.setText(randomized);
            confirmPassword.setText(randomized);
        }
    }

    private static boolean authenticatedBefore = false;
    void setLastAuth() {
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();

        long currentTime = new Date(System.currentTimeMillis()).getTime();
        prefsEditor.putLong(Constants.PREF_LAST_AUTH, currentTime);
        prefsEditor.apply();

        authenticatedBefore = true;
    }

    boolean shouldAskAuth() {
        int freq = MasterPassAuthentication.getAuthenticateInterval();

        if (freq == MasterPassAuthentication.EVERY_TIME) return true;
        if (freq == MasterPassAuthentication.ONE_TIME) return !authenticatedBefore;

        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        long lastAuth = appSharedPrefs.getLong(Constants.PREF_LAST_AUTH, 0);
        Date lastAuthDate = new Date(lastAuth);
        Date currentDate = new Date(System.currentTimeMillis());
        long minutesApart = MasterPassAuthentication.diffBetweenTwoDates(lastAuthDate, currentDate);

        long interval = 0;
        if (freq == MasterPassAuthentication.ONE_MIN) {
            interval = 1;
        } else if (freq == MasterPassAuthentication.FIVE_MIN) {
            interval = 5;
        }

        return minutesApart > interval;
    }

    boolean checkMasterPasswordMatches(String enteredMasterkey) {
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());

        if (!appSharedPrefs.contains(Constants.PREF_MASTER) || !appSharedPrefs.contains(Constants.PREF_MASTER_IV)) return false;

        String encryptedMaster = appSharedPrefs.getString(Constants.PREF_MASTER, "");
        String masterIv = appSharedPrefs.getString(Constants.PREF_MASTER_IV, "");

        byte[] encryptedMasterBytes = Base64.decode(encryptedMaster, Base64.DEFAULT);
        byte[] masterIvBytes = Base64.decode(masterIv, Base64.DEFAULT);
        String decryptedMaster = decryptText(Constants.ALIAS_CRYPTO, encryptedMasterBytes, masterIvBytes);

        return enteredMasterkey.equals(decryptedMaster);
    }

    void onSuccessfulAuthentication() {
        canEdit = true;

        editButton.setText(R.string.edit_copy);
        saveButton.setEnabled(true);

        confirmPasswordLabel.setVisibility(View.VISIBLE);
        confirmPassword.setVisibility(View.VISIBLE);
        confirmPassword.setEnabled(true);
        confirmPassword.setFocusableInTouchMode(true);

        password.setEnabled(true);
        password.setFocusableInTouchMode(true);
        website.setEnabled(true);
        website.setFocusableInTouchMode(true);

        byte[] encryptedBytes = (Base64.decode(encryptedPass, Base64.DEFAULT));
        String decryptedPass = decryptText(Constants.ALIAS_CRYPTO, encryptedBytes, iv);

        password.setText(decryptedPass);
        confirmPassword.setText(decryptedPass);
        password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        confirmPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);

        oldPassword = decryptedPass;

        editButton.setOnClickListener(null);
        editButton.setOnClickListener(this::onCopyClicked);
    }

    void saveDataToResultIntent() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(Constants.CREATE_PASS_NAME, name.getText().toString());
        resultIntent.putExtra(Constants.CREATE_PASS_WEBSITE, website.getText().toString());
        resultIntent.putExtra(Constants.CREATE_PASS_PASS, password.getText().toString());
        setResult(Activity.RESULT_OK, resultIntent);
    }

    @Override
    public boolean validateInput() {
        boolean result = true;
        String errorMsg = "";

        String inputSite = website.getText().toString();
        if (inputSite.isEmpty()) {
            result = false;
            errorMsg = getResources().getString(R.string.enter_pass_desc);
        }

        if (result) {
            String inputPass = password.getText().toString();
            String inputPassConfirm = confirmPassword.getText().toString();
            if (!inputPass.equals(inputPassConfirm)) {
                result = false;
                errorMsg = getResources().getString(R.string.passwords_wrong);
            }
        }

        // Show any error messages, if any
        if (!result) {
            Snackbar.make(saveButton, errorMsg, Snackbar.LENGTH_SHORT).show();
        }

        return result;
    }

    @Override
    public boolean hasUnsavedChanges() {
        return !oldDesc.equals(website.getText().toString())
                || !oldPassword.equals(password.getText().toString())
                || !oldPassword.equals(confirmPassword.getText().toString());
    }

    public String decryptText(String alias, byte[] stringToDecrypt, byte[] iv) {
        String result = "";
        try {
            result = (decryptor.decryptData(alias, stringToDecrypt, iv));
        } catch (UnrecoverableEntryException | NoSuchAlgorithmException |
                KeyStoreException | NoSuchPaddingException | IOException | InvalidKeyException e) {
        } catch (IllegalBlockSizeException | BadPaddingException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        return result;
    }
}
