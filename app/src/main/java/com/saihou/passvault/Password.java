package com.saihou.passvault;

import androidx.annotation.NonNull;

public class Password {
    // Data class

    String alias, desc;
    String encryptedPass; // Encoded in Base64
    byte[] iv;

    public Password(String alias, String desc, String pass, byte[] iv) {
        this.alias          = alias;
        this.desc           = desc;
        this.iv             = iv;
        this.encryptedPass  = pass;
    }

    String getIdentifier() {
        return alias;
    }

    String getDesc() {
        return desc;
    }

    String getEncryptedPass() {
        return encryptedPass;
    }

    byte[] getIv() {
        return  iv;
    }

    @NonNull
    @Override
    public String toString() {
        return "Identifier: " + alias + " | Desc: " + desc + " | Pass: " + encryptedPass;
    }
}
