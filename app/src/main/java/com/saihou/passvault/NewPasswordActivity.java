package com.saihou.passvault;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.android.material.snackbar.Snackbar;

public class NewPasswordActivity extends AppCompatActivity implements IPasswordModifier {

    @BindView(R.id.create_password)
    EditText password;
    @BindView(R.id.create_confirm_password)
    EditText passwordConfirm;
    @BindView(R.id.create_site)
    EditText website;
    @BindView(R.id.create_alias)
    EditText name;
    @BindView(R.id.btn_create_visibility)
    ImageButton visibilityButton;
    @BindView(R.id.btn_save_new_password)
    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);

        setTitle(R.string.title_create_new_password);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);
    }

    @Override
    public void onBackPressed() {
        // For the Android back button on device
        onCancelClicked();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // For the back button on the top left corner
            case android.R.id.home:
                onCancelClicked();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    @OnClick(R.id.btn_create_visibility)
    public void onVisibilityClicked() {
        int currentInputType = password.getInputType();
        boolean isCurrentlyHidden = currentInputType == (InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        int newInputType = isCurrentlyHidden ? InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD : (InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        visibilityButton.setImageResource(isCurrentlyHidden ? R.drawable.ic_visibility_24px : R.drawable.ic_visibility_off_24px);

        password.setInputType(newInputType);
        passwordConfirm.setInputType(newInputType);
    }

    @Override
    @OnClick(R.id.btn_create_randomize)
    public void onRandomizePasswordClicked() {
        String randomized = PasswordRandomizer.getRandomPassword();
        password.setText(randomized);
        passwordConfirm.setText(randomized);
    }

    @Override
    @OnClick(R.id.btn_save_new_password)
    public void onSaveClicked() {
        if (validateInput()) {
            saveDataToResultIntent();
            finish();
        }
    }

    @Override
    @OnClick(R.id.btn_cancel_new_password)
    public void onCancelClicked() {
        if (hasUnsavedChanges()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.cancel_new_pass);
            builder.setPositiveButton(R.string.yes, (dialog, which) -> {
                dialog.dismiss();
                finish();
            });

            builder.setNegativeButton(R.string.no, (dialog, which) -> {
                // Do nothing
                dialog.dismiss();
            });

            AlertDialog alert = builder.create();
            alert.show();
        } else {
            finish();
        }
    }

    void saveDataToResultIntent() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(Constants.CREATE_PASS_NAME, name.getText().toString());
        resultIntent.putExtra(Constants.CREATE_PASS_WEBSITE, website.getText().toString());
        resultIntent.putExtra(Constants.CREATE_PASS_PASS, password.getText().toString());
        setResult(Activity.RESULT_OK, resultIntent);
    }

    @Override
    public boolean validateInput() {
        boolean result = true;
        String errorMsg = "";

        String inputName = name.getText().toString();
        if (inputName.isEmpty()) {
            result = false;
            errorMsg = getResources().getString(R.string.enter_pass_identifer);
        }

        if (result) {
            String inputSite = website.getText().toString();
            //if (!Patterns.WEB_URL.matcher(inputSite).matches()) {
            if (inputSite.isEmpty()) {
                result = false;
                errorMsg = getResources().getString(R.string.enter_pass_desc);
            }
        }

        if (result) {
            String inputPass = password.getText().toString();
            String inputPassConfirm = passwordConfirm.getText().toString();
            if (!inputPass.equals(inputPassConfirm)) {
                result = false;
                errorMsg = getResources().getString(R.string.passwords_wrong);
            }
        }


        // Show any error messages, if any
        if (!result) {
            Snackbar.make(saveButton, errorMsg, Snackbar.LENGTH_SHORT).show();
        }

        return result;
    }

    @Override
    public boolean hasUnsavedChanges() {
        return name.getText().toString().length() > 0 || website.getText().toString().length() > 0 ||
            password.getText().toString().length() > 0 || passwordConfirm.getText().toString().length() > 0;
    }
}
